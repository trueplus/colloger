package logger

import (
	"encoding/json"
	"log"
	"strings"
	"time"

	"github.com/google/uuid"
)

func genLogTransactionMap[T any](trn T) map[string]interface{} {
	s, _ := json.Marshal(trn)
	j := make(map[string]*json.RawMessage)
	_ = json.Unmarshal(s, &j)
	jj := make(map[string]*json.RawMessage, len(j))
	for k, v := range j {
		jj[strings.ToLower(k)] = v
	}
	ss, _ := json.Marshal(jj)
	y := make(map[string]interface{})
	_ = json.Unmarshal(ss, &y)
	return y
}

func (gnD *GeneralInfo) setTimestamp(start time.Time) {
	gnD.StartDate = formatDateTimeLog(start)
	gnD.EndDate = formatDateTimeLog(time.Now())
	gnD.ElapsedTime = float64(time.Since(start).Milliseconds())
}

func getValueString(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}

func (trn *LogTransaction) checkData() {
	if trn.TxID == "" {
		log.Printf("Warning: TxID is required, got %v", trn.TxID)
	}

	if trn.StepTxID == "" {
		log.Printf("Warning: StepTxID is required, got %v", trn.StepTxID)
	}
}

func (res *Result) setIndicator(logCat LogCategory) {
	var sInd ResultIndicator
	if logCat == ORDER {
		sInd = COMPLETED
	} else {
		sInd = SUCCESS
	}

	var fInd ResultIndicator
	if logCat == SECURITY_AUDIT {
		fInd = FAIL
	} else {
		fInd = FAILED
	}

	if res.Code == "200" {
		res.Indicator = sInd
	} else if res.Code == "202" {
		res.Indicator = INPROGRESS
	} else {
		res.Indicator = fInd
	}
}

func (trn *LogTransaction) prepare(start time.Time) {
	if trn.LogCategory == ORDER {
		trn.StepTxID = trn.TxID
	} else {
		trn.StepTxID = uuid.NewString()
	}
	trn.Result.setIndicator(trn.LogCategory)
	trn.GeneralInfo.setTimestamp(start)
}

func formatDateTimeLog(t time.Time) string {
	return t.Format("2006-01-02T15:04:05-0700")
}
