package logger

// result_indicator
type ResultIndicator string

const (
	COMPLETED  ResultIndicator = "COMPLETED"
	INPROGRESS ResultIndicator = "INPROGRESS"
	SUCCESS    ResultIndicator = "SUCCESS"
	FAILED     ResultIndicator = "FAILED"
	FAIL       ResultIndicator = "FAIL"
)

// log_cat
type LogCategory string

const (
	ORDER          LogCategory = "order"
	DETAIL         LogCategory = "detail"
	SECURITY_AUDIT LogCategory = "SecurityAudit"
)

// For Security audit service_type
type Activity string

const (
	VIEW   Activity = "View"
	CREATE Activity = "Create"
	DELETE Activity = "Delete"
	MODIFY Activity = "Modify"
	EXPORT Activity = "Export"
)
