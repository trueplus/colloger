package logger

type LogTransaction struct {
	GeneralInfo
	Result
}

type OrderTransaction struct {
	LogTransaction
	RequestResponse
	Customer
	Certificate
	Transaction
	Msisdn      string `json:"msisdn"`
	SearchKey   string `json:"search_key"`
	DealerCode  string `json:"dealer_code"`
	EmployeeID  string `json:"employee_id"`
	ProductInfo string `json:"product_info"`
}

type StepTransaction struct {
	LogTransaction
	Step
	HttpRequest
}

type SecurityAuditTransaction struct {
	LogTransaction
	HttpRequest
	RequestResponse
	EmployeeID string `json:"employee_id"`
}

type GeneralInfo struct {
	Identification
	Project
	Team        string      `json:"@team"`
	Suffix      string      `json:"@suffix"`
	Product     string      `json:"product"`
	ServiceType string      `json:"service_type"`
	Channel     string      `json:"channel"`
	LogCategory LogCategory `json:"log_cat"`
	StartDate   string      `json:"start_date"`
	EndDate     string      `json:"end_date"`
	ElapsedTime float64     `json:"elapsed_time"`
}

type SearchInfo struct {
	Msisdn      string `json:"msisdn"`
	SearchKey   string `json:"search_key"`
	DealerCode  string `json:"dealer_code"`
	EmployeeID  string `json:"employee_id"`
	ProductInfo string `json:"product_info"`
}

type Identification struct {
	TxID     string `json:"txid"`
	RefID    string `json:"ref_id"`
	StepTxID string `json:"step_txid"`
}

type Result struct {
	Indicator   ResultIndicator `json:"result_indicator"`
	Code        string          `json:"result_code"`
	Description string          `json:"result_desc"`
}

func (r *Result) SetResult(code, desc string) {
	r.Code = code
	r.Description = desc
}

type RequestResponse struct {
	Request  string `json:"request"`
	Response string `json:"response"`
}

func (rr *RequestResponse) SetRequestResponse(req, resp string) {
	rr.Request = req
	rr.Response = resp
}

type Customer struct {
	Name      string `json:"customer_name"`
	Type      string `json:"customer_type"`
	SubType   string `json:"customer_subtype"`
	PricePlan string `json:"customer_priceplan"`
	ExtraInfo string `json:"customer_extra_info"`
}

type Certificate struct {
	ID   string `json:"certificate_id"`
	Type string `json:"certificate_type"`
}

type Campaign struct {
	Code string `json:"campaign_code"`
	Name string `json:"campaign_name"`
}

type Transaction struct {
	Data      string `json:"transaction_data"`
	ExtraInfo string `json:"transaction_extra_info"`
}

type Project struct {
	Code string `json:"project_code"`
	Name string `json:"project_name"`
}

type Step struct {
	Name     string `json:"step_name"`
	Request  string `json:"step_request"`
	Response string `json:"step_response"`
}

func (s *Step) SetStep(req, resp string) {
	s.Request = req
	s.Response = resp
}

type HttpRequest struct {
	Method       string `json:"method"`
	Endpoint     string `json:"endpoint"`
	EndpointName string `json:"endpoint_name"`
	Headers      string `json:"header"`
}

func (h *HttpRequest) SetHttpRequest(method, endpoint, endpointName, headers string) {
	h.Method = method
	h.Endpoint = endpoint
	h.EndpointName = endpointName
	h.Headers = headers
}

type LogConfig struct {
	Team        string
	Suffix      string
	System      string
	Product     string
	ServiceType string
	Channel     string
}
