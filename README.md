# Col-Logger Project

[![Go Reference](https://pkg.go.dev/badge/golang.org/x/example.svg)](https://pkg.go.dev/golang.org/x/example)

This repository is a standard logging library for monitoring purpose..

## Setup the project

### Dockerfile

Add to Dockerfile

```
RUN echo "machine gitlab.com login git-rcd-os password {personal access token}" > ~/.netrc
```

### Mac os

Run on terminal

```
echo "machine gitlab.com login git-rcd-os password {personal access token}" > ~/.netrc
```

### Windows

This will let Git authenticate on HTTPS using .netrc:

* The file should be named _netrc and located in `c:\Users\<username>`
* You will need to set an environment variable called `HOME=%USERPROFILE%` (set system-wide environment variables using the System option in the control panel. Depending on the version of Windows, you may need to select "Advanced Options".).
* The password stored in the `_netrc` file cannot contain spaces (quoting the password will not work).

## Install module

```
go get gitlab.com/true-itsd/bss/cm/c-misc-auth-api/col-logger@v1.0.0 
```

## Example

**Setup log config**

```
colLogger := logger.LogConfig{
  Team:        "team",
  Suffix:      "Suffix",
  System:      "System",
  Product:     "Product",
  ServiceType: "ServiceType",
  Channel:     "Channel",
 }
```

**Log details**

```
stepTrn := logger.GetStepTransaction(req.StepName, logGnr)
 defer func() {
  stepTrn.SetHttpRequest(data.Method, data.Endpoint, "")
  stepTrn.SetStep(data.Request, data.Response)
  stepTrn.SetResult(data.ResultCode, data.ResultDesc)
  stepTrn.WriteLog(start)
 }()
```

**Log order**

```
orderTrn := logger.GetOrderTransaction(logGnr,searchInfo)
 defer func() {
  orderTrn.SetRequestResponse(data.Request, data.Response)
  orderTrn.SetResult(data.ResultCode, data.ResultDesc)
  orderTrn.WriteLog(start)
 }()
```

**Log Security Audit**

```
auditTrn := logger.GetSecurityAuditTransaction(constants.VIEW, logGnr)
 defer func() {
  auditTrn.SetHttpRequest(data.Method, data.Endpoint, "")
  auditTrn.SetRequestResponse(data.Request, data.Response)
  auditTrn.SetResult(data.ResultCode, data.ResultDesc)
  auditTrn.WriteLog(start)
 }()
```
