package logger

import (
	"log"
	"os"
	"time"

	logr "github.com/sirupsen/logrus"
)

func Init() {
	logr.SetFormatter(&logr.JSONFormatter{})
	logr.SetOutput(os.Stdout)
}

func WriteLog[T any](trn T, indicator ResultIndicator) {
	m := genLogTransactionMap(trn)
	logr.WithFields(m).Info(indicator)
}

func LogError(err error, gnD GeneralInfo) {
	trn := GetStepTransaction("ERROR", gnD)
	trn.TxID = ""
	trn.Indicator = FAILED
	trn.Code = "500"
	trn.Description = err.Error()
	trn.setTimestamp(time.Now())
	log.Println("LOG ERROR", "-", FAILED)
	WriteLog(trn, trn.Indicator)
}

func (trn *OrderTransaction) WriteLog(start time.Time) {
	trn.prepare(start)
	trn.checkData()
	log.Println("LOG", ORDER, "-", trn.Indicator)
	WriteLog(trn, trn.Indicator)
}

func (trn *StepTransaction) WriteLog(start time.Time) {
	trn.prepare(start)
	trn.checkData()
	log.Println("LOG", DETAIL, "-", trn.Indicator, "-", trn.Step.Name)
	WriteLog(trn, trn.Indicator)
}

func (trn *SecurityAuditTransaction) WriteLog(start time.Time) {
	trn.prepare(start)
	trn.checkData()
	log.Println("LOG", SECURITY_AUDIT, "-", trn.Indicator)
	WriteLog(trn, trn.Indicator)
}

func GetGeneralInfo(txId string, cfg LogConfig) GeneralInfo {
	return GeneralInfo{
		Team:        cfg.Team,
		Suffix:      cfg.Suffix,
		Product:     cfg.Product,
		ServiceType: cfg.ServiceType,
		Channel:     cfg.Channel,
		Project: Project{
			Name: cfg.System,
		},
		Identification: Identification{
			TxID: txId,
		},
	}
}

func GetOrderTransaction(gnr GeneralInfo, searchInfo SearchInfo) (trn OrderTransaction) {
	trn.GeneralInfo = gnr
	trn.LogCategory = ORDER
	trn.SearchKey = searchInfo.SearchKey
	trn.Msisdn = searchInfo.Msisdn
	trn.DealerCode = searchInfo.DealerCode
	return
}

func GetStepTransaction(name string, gnr GeneralInfo) (trn StepTransaction) {
	trn.GeneralInfo = gnr
	trn.Name = name
	trn.LogCategory = DETAIL
	return
}

func GetSecurityAuditTransaction(act Activity, gnr GeneralInfo) (trn SecurityAuditTransaction) {
	trn.GeneralInfo = gnr
	trn.LogCategory = SECURITY_AUDIT
	trn.ServiceType = string(act)
	return
}
